import requests
import time
class API(object):
    
    @staticmethod
    async def get_stations() -> list:
        retry_num = 0
        try:
            return requests.get('http://api.gios.gov.pl/pjp-api/rest/station/findAll').json()
        except Exception as e:
            time.sleep(1)
            retry_num += 1
            print(e)
            print(f'{retry_num} fetch retry...')
            return await API.get_stations()
    
    @staticmethod
    async def get_measuring_stands_for_station(station_id:int) -> list:
        retry_num = 0
        try:
            return requests.get(f'http://api.gios.gov.pl/pjp-api/rest/station/sensors/{station_id}').json()
        except Exception as e:
            time.sleep(1)
            retry_num += 1
            print(e)
            print(f'{retry_num} fetch retry...')
            return await API.get_measuring_stands_for_station(station_id)

    @staticmethod
    async def get_measuring_stand_data(stand_id:int) -> list:
        retry_num = 0
        try:
            return requests.get(f"http://api.gios.gov.pl/pjp-api/rest/data/getData/{stand_id}").json()
        except Exception as e:
            time.sleep(1)
            retry_num += 1
            print(e)
            print(f'{retry_num} fetch retry...')
            return await API.get_measuring_stand_data(stand_id)
