from api import API
from pprint import pprint
import matplotlib.pyplot as plt
from datetime import datetime
import timeit
import time
import asyncio
from db_controller import Database


async def get_stations_names_list() -> None:
    station_list = await API.get_stations()
    print([station['stationName'] for station in station_list])

async def get_stations_ids_list() -> list:
    station_list = await API.get_stations()
    return [station['id'] for station in station_list]


async def get_measuring_stands_list_for_station(station_id:int):
    stands_list = await API.get_measuring_stands_for_station(station_id)
    info = [stand['param']['paramName'] + " -> id: " + f"{stand['id']}" for stand in stands_list]
    print(info)


async def get_all_stand_data() -> dict:
    station_ids_list = await get_stations_ids_list()
    print('loading data...')
    data = {}
    for id in station_ids_list:
        print(f'getting {id} station data...')
        stand_data = await get_all_stand_data_for_station(id)
        data[id] = stand_data
    pprint(data)
    return data


async def get_all_stand_data_for_station(station_id:int) -> list:
    stands_list = await API.get_measuring_stands_for_station(station_id)
    stand_id_list = [stand['id'] for stand in stands_list]
    data_for_stand = []
    for id in stand_id_list:
        element = await API.get_measuring_stand_data(id)
        data_for_stand.append(element)
    # pprint(data_for_stand)
    return data_for_stand

def get_specific_stand_data(*args:int) -> None:
    data_for_stand = [API.get_measuring_stand_data(id) for id in args]
    pprint(data_for_stand)


async def get_all_measuring_stands_list() -> None:
    station_list = await API.get_stations()
    result = []
    print('loading data...')
    for station in station_list:
        measuring_stands = await API.get_measuring_stands_for_station(station['id'])
        stands_list = [ (stand["id"], stand['param']['paramName']) for stand in measuring_stands]
        info = f'{[station["id"]]}{ station["stationName"]}: { ", ".join([f"[{stand[0]}]{stand[1]}" for stand in stands_list]) }'
        result.append(info)
    for info in result:
        print(info)


def show_station_data_chart(station_id:int) -> None:
    stand_data = get_all_stand_data_for_station(station_id)
    for stand in stand_data:
        values = []
        dates = []
        for element in stand['values']:
            date_str = element['date']
            date_obj = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
            values.append(element['value'])
            dates.append(date_obj)

        data = {'dates': dates, 'values': values}
        plt.plot('dates', 'values', data=data, label=stand['key'])

    plt.xlabel('Czas')
    plt.ylabel('Wartość wskaźnika')
    plt.legend()
    plt.show()

# show_station_data_chart(52)


# asyncio.run(get_all_stand_data_for_station(52))

asyncio.run(get_all_stand_data())
# Database.save_to_db(data)
